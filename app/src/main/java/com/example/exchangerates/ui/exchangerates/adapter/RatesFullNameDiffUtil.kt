package com.example.exchangerates.ui.exchangerates.adapter

import androidx.recyclerview.widget.DiffUtil
import com.example.exchangerates.model.RatesFullName

class RatesFullNameDiffUtil : DiffUtil.ItemCallback<RatesFullName>() {


    override fun areContentsTheSame(oldItem: RatesFullName, newItem: RatesFullName): Boolean {
        return oldItem.ratesAbr == newItem.ratesAbr
    }

    override fun areItemsTheSame(oldItem: RatesFullName, newItem: RatesFullName): Boolean {
        return oldItem == newItem
    }

}