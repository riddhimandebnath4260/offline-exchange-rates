package com.example.exchangerates.ui.exchangerates

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.exchangerates.BuildConfig
import com.example.exchangerates.data.repos.rates.RatesRepositoryFact
import com.example.exchangerates.data.source.local.database.RatesDatabase
import com.example.exchangerates.data.source.local.database.RatesFullNameDatabase
import com.example.exchangerates.data.source.remote.NetworkClient
import com.example.exchangerates.databinding.ActivityExchangerateBinding
import com.example.exchangerates.helper.ResolveApiResponse
import com.example.exchangerates.helper.extensions.viewModelProvider
import com.example.exchangerates.model.RatesFullName
import com.example.exchangerates.ui.exchangerates.adapter.RatesAdapter
import kotlinx.coroutines.flow.Flow


class ExchangeRateActivity : AppCompatActivity() {
    private lateinit var binding: ActivityExchangerateBinding

    private lateinit var viewModel: ExchangeRateViewModel

    private lateinit var ratesAdapter: RatesAdapter
    private lateinit var fullNameRates: Flow<List<RatesFullName>>


    private var mBase: String? = "USD"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityExchangerateBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Dependency Injection
        // Retrofit API Service
        val ratesApiService = NetworkClient.getRatesApiService(BuildConfig.BASE_URL)
        // Repository && Database
        val ratesFullNameDatabase = RatesFullNameDatabase.getDatabase(applicationContext)
        val ratesDatabase = RatesDatabase.getRateDatabase(applicationContext)
        val repository = RatesRepositoryFact.getInstance(ratesApiService, ResolveApiResponse,ratesFullNameDatabase,ratesDatabase)
        // ViewModel
        val viewModelFactory = ExchangeRateViewModelFactory(repository,ratesFullNameDatabase,ratesDatabase)
        viewModel = viewModelProvider(viewModelFactory)

        fullNameRates = ratesFullNameDatabase.exchangeRateFullNameDao().readFullName()

        ratesAdapter = RatesAdapter()
        binding.ratesList.layoutManager = LinearLayoutManager(this)
        binding.ratesList.adapter = ratesAdapter

        viewModel.loading.observe(this) {
            if (it) {
                binding.progressBar.show()
            } else {
                binding.progressBar.hide()
            }
        }

        viewModel.errorResult.observe(this) {
            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
        }

        viewModel.rates.observe(this) {
            ratesAdapter.submitList(it.toList())
            Log.d("test",it.toString())
        }

        viewModel.loadRatesList(base = mBase)
        viewModel.loadRatesFullName(true,true,true)

    }
}

