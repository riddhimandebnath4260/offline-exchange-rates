package com.example.exchangerates.ui.exchangerates

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.exchangerates.data.repos.rates.RatesRepository
import com.example.exchangerates.data.source.local.database.RatesDatabase
import com.example.exchangerates.data.source.local.database.RatesFullNameDatabase

class ExchangeRateViewModelFactory(
    private val repository: RatesRepository,
    private val ratesFullNameDatabase: RatesFullNameDatabase,
    private val ratesDatabase: RatesDatabase
) :ViewModelProvider.Factory{
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(ExchangeRateViewModel::class.java)) {
            ExchangeRateViewModel(repository,ratesFullNameDatabase,ratesDatabase) as T
        } else {
            throw IllegalArgumentException("ViewModel Not Found")
        }
    }

}
