package com.example.exchangerates.ui.exchangerates

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.exchangerates.BuildConfig
import com.example.exchangerates.data.repos.rates.RatesRepository
import com.example.exchangerates.data.source.local.database.RatesDatabase
import com.example.exchangerates.data.source.local.database.RatesFullNameDatabase
import com.example.exchangerates.helper.Resource
import com.example.exchangerates.helper.Results
import com.example.exchangerates.model.RatesEntity
import com.example.exchangerates.model.RatesFullName
import com.example.exchangerates.model.RatesList
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch

class ExchangeRateViewModel(
    private val repository: RatesRepository,
    private val ratesFullNameDatabase: RatesFullNameDatabase,
    ratesDatabase: RatesDatabase
):ViewModel() {

    private val _loading = MutableLiveData<Boolean>()
    val loading: LiveData<Boolean> = _loading

    private val _rates = MutableLiveData<Map<String,Double>>()
    val rates: LiveData<Map<String,Double>> = _rates

    private val _ratesFullName = MutableLiveData<List<RatesFullName>>()
    val ratesFullName: LiveData<List<RatesFullName>> = _ratesFullName

    private val _errorResult = MutableLiveData<String>()
    val errorResult: LiveData<String> = _errorResult

    fun loadRatesList(
        base: String?=null,
        symbols:String?=null,
        prettyPrint: Boolean?=null,
        showAlternative: Boolean?=null
    ){
        viewModelScope.launch{
            _loading.postValue(true)

            val result = repository.loadRates(
                appId = BuildConfig.API_TOKEN,
                base = base,
                symbols = symbols,
                prettyPrint = prettyPrint,
                showAlternative = showAlternative,
            )

            Log.d(TAG,result.toString())

            _rates.postValue(result)
        }
    }


    companion object {
        private const val TAG = "RatesViewModel"
    }

    fun loadRatesFullName(prettyPrint: Boolean?, showAlternative: Boolean?, showInactive: Boolean?) {


        viewModelScope.launch {



            val result = repository.loadRatesFullName(prettyPrint, showAlternative, showInactive)

            Log.d("fullname",result.toString())

            _ratesFullName.postValue(result)



        }
    }
}

@JvmName("postValueT")
private fun <T> MutableLiveData<T>.postValue(result: Flow<Resource<List<RatesEntity>>>) {

}


private fun <T> MutableLiveData<T>.postValue(result: Flow<Resource<T>>) {

}

