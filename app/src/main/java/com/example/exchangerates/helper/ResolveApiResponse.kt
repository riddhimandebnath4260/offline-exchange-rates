package com.example.exchangerates.helper

import android.util.Log


object ResolveApiResponse {

    suspend fun <T> resolve(classTag: String, updateCall: suspend () -> T) =
        try {
            Results.Success(updateCall.invoke())
        } catch (throwable: Throwable) {
            Log.w(classTag, throwable)
            throwable.printStackTrace()
            Results.Failure(throwable)
        }
}
