package com.example.exchangerates.helper

import androidx.room.TypeConverter
import com.google.gson.Gson

import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type


class MapConverter {
    @TypeConverter
    fun fromString(value: String?): Map<String?, Double?>? {
        val mapType: Type = object : TypeToken<Map<String?, Double?>?>() {}.type
        return Gson().fromJson(value, mapType)
    }

    @TypeConverter
    fun fromStringMap(map: Map<String?, Double?>?): String? {
        val gson = Gson()
        return gson.toJson(map)
    }
}