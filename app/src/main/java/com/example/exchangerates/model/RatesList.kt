package com.example.exchangerates.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName


data class RatesList(
    @field:SerializedName("disclaimer")
    val disclaimer : String?= null,
    @field:SerializedName("license")
    val license : String?= null,
    @field:SerializedName("timestamp")
    val timestamp: Int = 0,
    @field:SerializedName("base")
    val base: String? = null,
    @field:SerializedName("rates")
    val rates: Map<String,Double>
)
