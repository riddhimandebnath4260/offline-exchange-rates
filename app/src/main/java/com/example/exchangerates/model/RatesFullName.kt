package com.example.exchangerates.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Rates_Fullname")
data class RatesFullName(
    @PrimaryKey
    val ratesAbr: String,
    val ratesFullname:String
)

