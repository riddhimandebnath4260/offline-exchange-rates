package com.example.exchangerates.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Rates")
data class RatesEntity(
    @PrimaryKey
    val code: String,
    val rate: Double
)
