package com.example.exchangerates.data.source.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.exchangerates.model.RatesFullName
import kotlinx.coroutines.flow.Flow

@Dao
interface ExchangeRateFullNameDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addFullName(fullName: List<RatesFullName>)

    @Query("SELECT * FROM Rates_Fullname")
    fun readFullName():Flow<List<RatesFullName>>

    @Query("DELETE FROM Rates_Fullname")
    suspend fun deleteAllRatesFullName()
}