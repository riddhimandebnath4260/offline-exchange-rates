package com.example.exchangerates.data.source.local.database

import android.content.Context
import androidx.room.*
import com.example.exchangerates.data.source.local.dao.ExchangeRateDao
import com.example.exchangerates.data.source.local.dao.ExchangeRateFullNameDao
import com.example.exchangerates.helper.MapConverter
import com.example.exchangerates.model.RatesEntity
import com.example.exchangerates.model.RatesList

@Database(entities = [RatesEntity::class], version = 1)
@TypeConverters(MapConverter::class)
abstract class RatesDatabase:RoomDatabase() {

    abstract fun exchangeRateDao():ExchangeRateDao

    companion object{
        @Volatile
        private var INSTANCE: RatesDatabase?=null

        fun getRateDatabase(context: Context): RatesDatabase {
            val tempInstance = INSTANCE
            if (tempInstance!=null){
                return tempInstance
            }
            synchronized(this){
                val dataBase = Room.databaseBuilder(context.applicationContext,
                    RatesDatabase::class.java,"Rates").allowMainThreadQueries().build()
                INSTANCE = dataBase
                return dataBase
            }
        }
    }

}