package com.example.exchangerates.data.source.local.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.exchangerates.data.source.local.dao.ExchangeRateFullNameDao
import com.example.exchangerates.model.RatesFullName

@Database(entities = [RatesFullName::class], version = 1)

abstract class RatesFullNameDatabase:RoomDatabase() {

    abstract fun exchangeRateFullNameDao(): ExchangeRateFullNameDao

    companion object{
        @Volatile
        private var INSTANCE: RatesFullNameDatabase?=null

        fun getDatabase(context: Context): RatesFullNameDatabase {
            val tempInstance = INSTANCE
            if (tempInstance!=null){
                return tempInstance
            }
            synchronized(this){
                val dataBase = Room.databaseBuilder(context.applicationContext,
                    RatesFullNameDatabase::class.java,"Rates_Fullname").allowMainThreadQueries().build()
                INSTANCE = dataBase
                return dataBase
            }
        }
    }

}