package com.example.exchangerates.data.repos.rates

import com.example.exchangerates.helper.Resource
import com.example.exchangerates.model.RatesEntity
import com.example.exchangerates.model.RatesFullName
import com.example.exchangerates.model.RatesList
import kotlinx.coroutines.flow.Flow

interface RatesRepository {

    suspend fun loadRates(
        appId : String,
        base : String?,
        symbols : String?,
        prettyPrint : Boolean?,
        showAlternative : Boolean?
    ) : Flow<Resource<List<RatesEntity>>>

    suspend fun loadRatesFullName(
        prettyPrint: Boolean?,
        showAlternative: Boolean?,
        showInactive:Boolean?): Flow<Resource<List<RatesFullName>>>


}