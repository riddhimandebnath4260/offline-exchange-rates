package com.example.exchangerates.data.repos.rates

import androidx.room.withTransaction
import com.example.exchangerates.data.source.local.database.RatesDatabase
import com.example.exchangerates.data.source.local.database.RatesFullNameDatabase
import com.example.exchangerates.data.source.remote.RatesApiService
import com.example.exchangerates.helper.ResolveApiResponse
import com.example.exchangerates.helper.Resource
import com.example.exchangerates.helper.networkBoundResource
import com.example.exchangerates.model.RatesEntity
import com.example.exchangerates.model.RatesFullName
import com.example.exchangerates.model.RatesList
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow

class RatesRepositoryImpl (
    private val ratesApiService: RatesApiService,
    private val resolveApiResponse: ResolveApiResponse,
    private val ratesFullNameDatabase: RatesFullNameDatabase,
    private val ratesDatabase: RatesDatabase,
) : RatesRepository {

    private val ratesInfo = ratesFullNameDatabase.exchangeRateFullNameDao()
    private val ratesCodeInfo = ratesDatabase.exchangeRateDao()


    override suspend fun loadRates(
        appId: String,
        base : String?,
        symbols : String?,
        prettyPrint : Boolean?,
        showAlternative : Boolean?
    ): Flow<Resource<List<RatesEntity>>>
    = networkBoundResource(
        query = {
           ratesCodeInfo.readRate()
        },
        fetch = {
            delay(2000)
            val response = ratesApiService.rates(appId, base, symbols, prettyPrint, showAlternative)
            response.body()


        },
        saveFetchResult = {response ->
            ratesDatabase.withTransaction {
                ratesCodeInfo.deleteAllRates()
                ratesCodeInfo.addRate(response!!.rates.map {
                    RatesEntity(it.key,it.value)
                })
            }
        }
    )


    override suspend fun loadRatesFullName(
        prettyPrint: Boolean?,
        showAlternative: Boolean?,
        showInactive: Boolean?
    ): Flow<Resource<List<RatesFullName>>> = networkBoundResource(
        query = {
            ratesInfo.readFullName()
        },
        fetch = {
            delay(2000)
            val response = ratesApiService.ratesFullName()
            response.body()
        },
        saveFetchResult = {response ->
            ratesFullNameDatabase.withTransaction {
                ratesInfo.deleteAllRatesFullName()
                ratesInfo.addFullName(response!!.map {
                    RatesFullName(it.key,it.value)
                })
            }
        }
    )



//   override suspend fun loadRatesFullName(
//        prettyPrint: Boolean?,
//        showAlternative: Boolean?,
//        showInactive: Boolean?
//    ) = withContext(Dispatchers.IO){
//        val result = ratesApiService.ratesFullName()
//        if (result.body()!=null){
//            ratesInfo.addFullName(result.body()!!.map { RatesFullName(it.key,it.value) })
//            Log.d("check",result.body().toString())
//        }
//    }




}
