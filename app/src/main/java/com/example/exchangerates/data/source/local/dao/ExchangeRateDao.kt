package com.example.exchangerates.data.source.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.exchangerates.model.RatesEntity
import com.example.exchangerates.model.RatesList
import kotlinx.coroutines.flow.Flow

@Dao
interface ExchangeRateDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addRate(fullName: List<RatesEntity>)

    @Query("SELECT * FROM Rates")
    fun readRate(): Flow<List<RatesEntity>>

    @Query("DELETE FROM Rates")
    suspend fun deleteAllRates()
}